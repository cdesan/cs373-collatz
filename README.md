# CS 373: Software Engineering Collatz Repo

* Name: Carlo DeSantis

* EID: ccd2238

* GitLab ID: cdesan

* HackerRank ID: cdesantis

* Git SHA: ac4dcec83461b2b01a2dca75b32f36a5692a023c

* GitLab Pipelines: https://gitlab.com/cdesan/cs373-collatz/-/pipelines

* Estimated completion time: 5

* Actual completion time: 7

* Comments: Took longer than I expected, but I enjoyed the challenge.
