#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, Tuple, Dict

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> Tuple[int, int]:
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return int(a[0]), int(a[1])


# ------------
# collatz_eval
# ------------


def collatz_eval(t: Tuple[int, int]) -> Tuple[int, int, int]:
    """
    t a tuple of two ints
    return a tuple of three ints
    """
    i, j = t

    assert i > 0
    assert i < 1000000
    assert j > 0
    assert j < 1000000

    result = 1

    # Store inital vals for output
    initial_i = i
    initial_j = j

    # B,E,M optimization suggested by Prof. Downing
    m = (j // 2) + 1
    if i < m:
        i = m

    # Make sure we have the values in the right order for iteration
    if i > j:
        i = j
        j = initial_i

    assert i <= j

    # Count is equivalent to the path for collatz
    for val in range(i, j + 1):
        # Storage for intermediary numbers we come across
        intermediary_nums = []
        # Check cache
        if val in cacheDict:
            count = cacheDict[val]
        else:
            count = 1
            initial_val = val
            while val != 1 and val not in cacheDict:
                if (val % 2) == 0:
                    count += 1
                    val = val >> 1
                else:
                    count += 2
                    val = (val << 1) + val + 1
                    intermediary_nums.append(val)
                    # Skip next even number
                    val = val >> 1
                intermediary_nums.append(val)

            # Check if we terminated the loop because the value is in the cache
            if val in cacheDict:
                count = count + cacheDict[val] - 1
            # Update the cache with the target value
            cacheDict[initial_val] = count

        # Loop back through the intermediary vals and store their values as well
        cnt = count - 1
        for num in intermediary_nums:
            cacheDict[num] = cnt
            cnt -= 1

        if count > result:
            result = count

    assert result > 0
    return initial_i, initial_j, result


# -------------
# collatz_print
# -------------


def collatz_print(sout: IO[str], t: Tuple[int, int, int]) -> None:
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------

cacheDict: Dict[int, int] = {}


def collatz_solve(sin: IO[str], sout: IO[str]) -> None:
    """
    sin  a reader
    sout a writer
    """
    # Eager cache
    collatz_eval((1, 999999))

    for s in sin:
        collatz_print(sout, collatz_eval(collatz_read(s)))
